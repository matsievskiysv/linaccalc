# Dispersion solver

Dispersion solver features two types of equations: simple and advanced.

Simple equation
$$\frac{f}{f_0}=\sqrt{\frac{1-K^E \cos{\varphi}}{1+K^H \cos{\varphi}}}$$
Advanced equation
$$\frac f {f_0}
	=
	\sqrt{
		\frac{
			1 - K^E \cos{\varphi}\cosh{(\alpha D)}
		}{
			1 + K^H \cos{\varphi}\cosh{(\alpha D)}
		}
	}
$$

$$\cosh{(\alpha D)} =
	\frac{
		-(K^E - K^H) \cos{\varphi}
		\pm
		\left\{
			{(K^E - K^H)}^2 \cos^2{\varphi} +
			+
			4 (Q^2 {(K^E + K^H)}^2 \sin^2{\varphi} +
			K^E K^H \cos^2{\varphi})
			(1 + Q^2 {(K^E + K^H)}^2
			\sin^2{\varphi})
		\right\}^{\frac{1}{2}}
	}{
		2 Q^2 {(K^E + K^H)}^2 \sin^2{\varphi} +
		K^H K^E \cos^2{\varphi}
	}
$$

Set cell frequency button is a convenience function which sets currently selected cell frequency according to operation
mode and generator frequency.

## Equation derivation

### Simple equation. Capacitive normalization

$$- \frac{K^{E} X_{L}}{2} - \frac{K^{E} X_{R}}{2} - \frac{K^{H} X_{L} f^{2}}{2 f_{0}^{2}} - \frac{K^{H} X_{R} f^{2}}{2
f_{0}^{2}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} = 0$$
$$- \frac{K^{E} X_{0} \left(i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2} - \frac{K^{E}
X_{R}}{2} - \frac{K^{H} X_{0} f^{2} \left(i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2
f_{0}^{2}} - \frac{K^{H} X_{R} f^{2}}{2 f_{0}^{2}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} = 0$$
$$- \frac{K^{E} X_{0} \left(- i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2} - \frac{K^{E} X_{0}
\left(i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2} - \frac{K^{H} X_{0} f^{2} \left(- i
\sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2 f_{0}^{2}} - \frac{K^{H} X_{0} f^{2} \left(i
\sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2 f_{0}^{2}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} =
0$$
$$- K^{E} \cos{\left(\varphi \right)} - \frac{K^{H} f^{2} \cos{\left(\varphi \right)}}{f_{0}^{2}} -
\frac{f^{2}}{f_{0}^{2}} + 1 = 0$$
$$\frac{f}{f_0}=\sqrt{\frac{1-K^E \cos{\varphi}}{1+K^H \cos{\varphi}}}$$

### Simple equation. Inductive normalization

$$\frac{K^{E} X_{L} f_{0}^{2}}{2 f^{2}} + \frac{K^{E} X_{R} f_{0}^{2}}{2 f^{2}} + \frac{K^{H} X_{L}}{2} + \frac{K^{H}
X_{R}}{2} + X_{0} - \frac{X_{0} f_{0}^{2}}{f^{2}} = 0$$
$$\frac{K^{E} X_{0} f_{0}^{2} \left(i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2 f^{2}} +
\frac{K^{E} X_{R} f_{0}^{2}}{2 f^{2}} + \frac{K^{H} X_{0} \left(i \sin{\left(\varphi \right)} + \cos{\left(\varphi
\right)}\right)}{2} + \frac{K^{H} X_{R}}{2} + X_{0} - \frac{X_{0} f_{0}^{2}}{f^{2}} = 0$$
$$\frac{K^{E} X_{0} f_{0}^{2} \left(- i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2 f^{2}} +
\frac{K^{E} X_{0} f_{0}^{2} \left(i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2 f^{2}} +
\frac{K^{H} X_{0} \left(- i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2} + \frac{K^{H} X_{0}
\left(i \sin{\left(\varphi \right)} + \cos{\left(\varphi \right)}\right)}{2} + X_{0} - \frac{X_{0} f_{0}^{2}}{f^{2}} =
0$$
$$\frac{K^{E} f_{0}^{2} \cos{\left(\varphi \right)}}{f^{2}} + K^{H} \cos{\left(\varphi \right)} + 1 -
\frac{f_{0}^{2}}{f^{2}} = 0$$
$$\frac{f}{f_0}=\sqrt{\frac{1-K^E \cos{\varphi}}{1+K^H \cos{\varphi}}}$$
