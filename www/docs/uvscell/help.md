# Static solver

Static solver calculates distribution of the complex accelerating field by solving a system of equations
$$[A][\dot{x}]=[b_g]+[b_b]$$

Here $[A]$ matrix describes cell properties and interconnections, vector $[b_g]$ represents external power generator,
vector $[b_b]$ represents beam loading and vector $[\dot{x}]$ is a normalized amplitude vector.

Matrix elements $A_{jj}$ represent cell $j$ parameters and have form
$$A_{jj} = 1 - \frac{f^{2}}{f_{j}^{2}} + i\frac{f}{f_{j}}\frac{1}{Q_{j}}(1+\chi_{g}+\chi_{load})$$
Matrix elements $A_{jk}, j \ne k$ represent connection between cells $j$ and $k$ and have form
$$A_{jk} = - \frac{K^{E}_{jk}}{2} - \frac{K^{H}_{jk}}{2}\frac{f^{2}}{f_{j} f_{k}}$$
Vector elements $b^g_j$ represent external power input and have form
$$b^g_j=\frac{4 i \sqrt{\pi} \sqrt{P_{g}}
\sqrt{\chi_{g}} f e^{i \varphi}}{\sqrt{Q_{j}} \sqrt{f_{j}}}$$
Vector elements $b^b_j$ represent beam loading and have form
$$b^b_j=i\sqrt{\frac{\pi f_0 R_{sh}}{Q_0}}I\exp{(i\varphi_b)}$$

Normalized amplitude $\dot{x}$ is converted to accelerating field using equation
$$U = - i\frac{\dot{x}}{2}\sqrt\frac{R_{sh}}{\pi Q f}$$


**Note:** `f0`, `Rsh`, `Ke` and `Kh` variables are multiplied by scaling factors.
See `Input` documentation page for details.

## Equation derivation

Equations may be derived in two ways: with capacitive and inductive normalizations.
These normalizations yield the same results in all modules except for the eigenvalue solver, where they are used for
calculation of sections with predominant electric or magnetic coupling.
For completeness sake both cases derivations are presented below.

### Cell amplitude. Capacitive normalization

```
            Ml                          Mr
   ...  <-------->	     <----------------------------->  ...
                     L              R          C
                 ^   ^   ^      +-------+     | |
...   --+-------/ \ / \ / \-----+       +-----+ +------+--  ...
        |          v   v        +-------+     | |      |
        |                                              |
     ---+--- Cl                                     ---+--- Cr
     ---+---                                        ---+---
        |                                              |
        |    +-------+   +-------+   -----    -----    |
...   --+----+       +---+       +--(---->)--(---->)---+--  ...
             +-------+   +-------+   -----    -----
                 Rg          Rl        Eg       Eb
```
**Single cell equivalent circuit**

Writing down Kirchhoff's circuit laws for the circuit

$$I_{0} \left(i L_{0} \omega + R_{0} + R_{g} + R_{load} - \frac{i}{C_{\Sigma0} \omega}\right) + I_{l} \left(i M_{l}
\omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega + \frac{i}{C_{r} \omega}\right) = U e^{i \varphi}$$
and replacing lumped element parameters with electrodynamic ones
$$U = \sqrt{8 P_g R_g}$$
$$I_{0} \left(i L_{0} \omega + R_{0} + R_{g} + R_{load} - \frac{i}{C_{\Sigma0} \omega}\right) + I_{l} \left(i M_{l} \omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega + \frac{i}{C_{r} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{g}} e^{i \varphi}$$
$$Q_0 = \frac{\omega_0 L}{R} = \frac{1}{\omega_0 C_{\Sigma} R_0}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} + R_{g} +
R_{load}\right) + I_{l} \left(i M_{l} \omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega +
\frac{i}{C_{r} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{g}} e^{i \varphi}$$
$$\chi_g = \frac{R_g}{R_0}$$
$$\chi_l = \frac{R_l}{R_0}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + I_{l} \left(i M_{l} \omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega +
\frac{i}{C_{r} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$K_H = 2\frac{M}{\sqrt{L_1 L_2}}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + I_{l} \left(\frac{i K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}} \omega}{2} + \frac{i}{C_{l}
\omega}\right) + I_{r} \left(\frac{i K^{H}_{r} \sqrt{L_{0}} \sqrt{L_{r}} \omega}{2} + \frac{i}{C_{r} \omega}\right) = 2
\sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$K_E = 2\frac{\sqrt{C_{\Sigma 1} C_{\Sigma 2}}}{C_2}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + I_{l} \left(\frac{i K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}} \omega}{2} + \frac{i K^{E}_{l}}{2
\sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} \omega}\right) + I_{r} \left(\frac{i K^{H}_{r} \sqrt{L_{0}} \sqrt{L_{r}}
\omega}{2} + \frac{i K^{E}_{r}}{2 \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}}
\sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$I = X \sqrt{C}$$
$$\sqrt{C_{\Sigma0}} X_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} +
R_{0} \chi_{g} + R_{0} \chi_{load} + R_{0}\right) + \sqrt{C_{{\Sigma}l}} X_{l} \left(\frac{i K^{H}_{l} \sqrt{L_{0}}
\sqrt{L_{l}} \omega}{2} + \frac{i K^{E}_{l}}{2 \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} \omega}\right) +
\sqrt{C_{{\Sigma}r}} X_{r} \left(\frac{i K^{H}_{r} \sqrt{L_{0}} \sqrt{L_{r}} \omega}{2} + \frac{i K^{E}_{r}}{2
\sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i
\varphi}$$
$$\omega = 2 \pi f$$
$$\omega_0 = 2 \pi f_0$$
$$\sqrt{C_{\Sigma0}} X_{0} \left(\frac{i Q_{0} R_{0} f}{f_{0}} - \frac{i Q_{0} R_{0} f_{0}}{f} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + \sqrt{C_{{\Sigma}l}} X_{l} \left(i \pi K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}} f + \frac{i
K^{E}_{l}}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} f}\right) + \sqrt{C_{{\Sigma}r}} X_{r} \left(i \pi K^{H}_{r}
\sqrt{L_{0}} \sqrt{L_{r}} f + \frac{i K^{E}_{r}}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} f}\right) = 2 \sqrt{2}
\sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$f = \frac{1}{2 \pi \sqrt{C L}}$$
$$\sqrt{C_{\Sigma0}} X_{0} \left(\frac{i Q_{0} R_{0} f}{f_{0}} - \frac{i Q_{0} R_{0} f_{0}}{f} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + \sqrt{C_{{\Sigma}l}} X_{l} \left(\frac{i K^{E}_{l}}{4 \pi \sqrt{C_{\Sigma0}}
\sqrt{C_{{\Sigma}l}} f} + \frac{i K^{H}_{l} f}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} f_{0} f_{l}}\right) +
\sqrt{C_{{\Sigma}r}} X_{r} \left(\frac{i K^{E}_{r}}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} f} + \frac{i K^{H}_{r}
f}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} f_{0} f_{r}}\right) = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}}
\sqrt{\chi_{g}} e^{i \varphi}$$
$$2 i \pi \sqrt{C_{\Sigma0}} f \left(\sqrt{C_{\Sigma0}} X_{0} \left(\frac{i Q_{0} R_{0} f}{f_{0}} - \frac{i Q_{0} R_{0}
f_{0}}{f} + R_{0} \chi_{g} + R_{0} \chi_{load} + R_{0}\right) + \sqrt{C_{{\Sigma}l}} X_{l} \left(\frac{i K^{E}_{l}}{4
\pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} f} + \frac{i K^{H}_{l} f}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}}
f_{0} f_{l}}\right) + \sqrt{C_{{\Sigma}r}} X_{r} \left(\frac{i K^{E}_{r}}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}}
f} + \frac{i K^{H}_{r} f}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} f_{0} f_{r}}\right)\right) = 4 \sqrt{2} i \pi
\sqrt{C_{\Sigma0}} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} f e^{i \varphi}$$
$$2 i \pi \sqrt{C_{\Sigma0}} f \left(\sqrt{C_{\Sigma0}} X_{0} \left(\frac{i f}{2 \pi C_{\Sigma0} f_{0}^{2}} - \frac{i}{2
\pi C_{\Sigma0} f} + \frac{\chi_{g}}{2 \pi C_{\Sigma0} Q_{0} f_{0}} + \frac{\chi_{load}}{2 \pi C_{\Sigma0} Q_{0}
f_{0}} + \frac{1}{2 \pi C_{\Sigma0} Q_{0} f_{0}}\right) + \sqrt{C_{{\Sigma}l}} X_{l} \left(\frac{i K^{E}_{l}}{4 \pi
\sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} f} + \frac{i K^{H}_{l} f}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} f_{0}
f_{l}}\right) + \sqrt{C_{{\Sigma}r}} X_{r} \left(\frac{i K^{E}_{r}}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} f} +
\frac{i K^{H}_{r} f}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} f_{0} f_{r}}\right)\right) = \frac{4 i \sqrt{\pi}
\sqrt{P_{g}} \sqrt{\chi_{g}} f e^{i \varphi}}{\sqrt{Q_{0}} \sqrt{f_{0}}}$$
$$- \frac{K^{E}_{l} X_{l}}{2} - \frac{K^{E}_{r} X_{r}}{2} - \frac{K^{H}_{l} X_{l} f^{2}}{2 f_{0} f_{l}} -
\frac{K^{H}_{r} X_{r} f^{2}}{2 f_{0} f_{r}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} + \frac{i X_{0} \chi_{g} f}{Q_{0}
f_{0}} + \frac{i X_{0} \chi_{load} f}{Q_{0} f_{0}} + \frac{i X_{0} f}{Q_{0} f_{0}} = \frac{4 i \sqrt{\pi} \sqrt{P_{g}}
\sqrt{\chi_{g}} f e^{i \varphi}}{\sqrt{Q_{0}} \sqrt{f_{0}}}$$
This equation contains only electrodynamic parameters and used in the solver.

### Normalized amplitude to accelerating field conversion. Capacitive normalization

$$\frac{R_{sh}}{Q}$$
$$R_{sh} = \frac{U^2}{P_{loss}}$$
$$Q = \frac{\omega_0 W}{P_{loss}}$$
$$\frac{R_{sh}}{Q} = \frac{U^{2}}{W \omega_{0}}$$
$$W = \frac{C U_0^2}{2}$$
$$\frac{R_{sh}}{Q} = \frac{2 U^{2}}{C U_{0}^{2} \omega_{0}}$$
$$\frac{R_{sh}}{Q} = \frac{2 C_{\sum}}{C^{2} \omega_{0}}$$
$$\frac{R_{sh}}{Q} = \frac{C_{\sum}}{\pi C^{2} f}$$

---

$$P_{g} = \frac{E^{2}}{8 R_{g}}$$
$$E = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{g}}$$

---

$$i \sqrt{C_{\sum}} E \omega$$
$$Q = \frac{1}{\omega_0 C_\Sigma R}$$
$$i \sqrt{C_{\sum}} E \omega = \frac{i E \omega}{\sqrt{Q} \sqrt{R} \sqrt{\omega_{0}}}$$
$$i \sqrt{C_{\sum}} E \omega = \frac{2 \sqrt{2} i \sqrt{P_{g}} \sqrt{R_{g}} \omega}{\sqrt{Q} \sqrt{R}
\sqrt{\omega_{0}}}$$
$$\chi = \frac{R_g}{R}$$
$$i \sqrt{C_{\sum}} E \omega = \frac{2 \sqrt{2} i \sqrt{P_{g}} \sqrt{\chi} \omega}{\sqrt{Q} \sqrt{\omega_{0}}}$$
$$\omega = 2 \pi f$$
$$\omega_0 = 2 \pi f_0$$
$$2 i \pi \sqrt{C_{\sum}} E f = \frac{4 i \sqrt{\pi} \sqrt{P_{g}} \sqrt{\chi} f}{\sqrt{Q} \sqrt{f_{0}}}$$

---

$$U = \frac{1}{j \omega C} I = \frac{1}{j \omega C} X \sqrt{C_\Sigma}$$
$$U = - \frac{i \sqrt{C_{\sum}} X}{C \omega}$$
$$U = - \frac{i \sqrt{\pi} \sqrt{R_{sh}} X \sqrt{f}}{\sqrt{Q} \omega}$$
$$\omega = 2 \pi f$$
$$\omega_0 = 2 \pi f_0$$
$$U = - \frac{i \sqrt{R_{sh}} X}{2 \sqrt{\pi} \sqrt{Q} \sqrt{f}}$$

This equation is used for converting normalized amplitude to the accelerating field.

### Beam loading. Capacitive normalization

```
             L              R          C
         ^   ^   ^      +-------+     | |
+-------/ \ / \ / \-----+       +-----+ +------+
|          V   V        +-------+     | |      |
|                                              |
|                                              |
|                       -----                  |
+----------------------(--~--)-----------------+
                        -----
                          Ib
```
**Serial scheme**

$$I \left(i L \omega + R - \frac{i}{C \omega}\right) = U$$
$$Q=\frac{\rho}{R}=\frac{\omega_0 L}{R}=\frac{1}{\omega_0 C R}$$
$$I \left(\frac{i Q R \omega}{\omega_{0}} - \frac{i Q R \omega_{0}}{\omega} + R\right) = U$$
$$- \frac{I \omega^{2}}{\omega_{0}^{2}} + I + \frac{i I \omega}{Q \omega_{0}} = \frac{i U \omega}{Q R \omega_{0}}$$
$$W=\frac{L I^2}{2}=\frac{X^2}{2}$$
$$I=\frac{X}{\sqrt{L}}$$
$$- \frac{X \omega^{2}}{\sqrt{L} \omega_{0}^{2}} + \frac{X}{\sqrt{L}} + \frac{i X \omega}{\sqrt{L} Q \omega_{0}} =
\frac{i U \omega}{Q R \omega_{0}}$$
$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i \sqrt{L} U \omega}{Q R
\omega_{0}}$$
$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i U \omega}{\sqrt{Q} \sqrt{R}
\omega_{0}^{\frac{3}{2}}}$$
$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i U
\omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}}$$

```
           ~
           L
       ^   ^   ^
  +---/ \ / \ / \---+
  |      V   V      |
  |        ~        |
  |        R        |
  |    +-------+    |
  +----+       +----+
  |    +-------+    |
  |        ~        |
  |        C        |
  |       | |       |
  +-------+ +-------+
  |       | |       |
  |                 |
  |      -----      |
  +-----(---->)-----+
         -----
		   ~
		   Ib
```
**Parallel scheme**

$$\hat{U} \left(i \hat{C} \omega + \frac{1}{\hat{R}} - \frac{i}{\hat{L} \omega}\right) = \hat{I}$$
$$Q=\frac{R}{\rho}=\frac{R}{\omega_0 L}=\omega_0 C R$$
$$\hat{U} \left(- \frac{i \hat{Q} \hat{\omega_0}}{\hat{R} \omega} + \frac{i \hat{Q} \omega}{\hat{R} \hat{\omega_0}} +
\frac{1}{\hat{R}}\right) = \hat{I}$$
$$\hat{U} - \frac{\hat{U} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{U} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\hat{I} \hat{R} \omega}{\hat{Q} \hat{\omega_0}}$$
$$W=\frac{C U^2}{2}=\frac{X^2}{2}$$
$$\hat U = \frac{\hat X}{\sqrt{\hat C}}$$
$$\frac{\hat{X}}{\sqrt{\hat{C}}} - \frac{\hat{X} \omega^{2}}{\sqrt{\hat{C}} \hat{\omega_0}^{2}} + \frac{i \hat{X}
\omega}{\sqrt{\hat{C}} \hat{Q} \hat{\omega_0}} = \frac{i \hat{I} \hat{R} \omega}{\hat{Q} \hat{\omega_0}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\sqrt{\hat{C}} \hat{I} \hat{R} \omega}{\hat{Q} \hat{\omega_0}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\hat{I} \sqrt{\hat{R}} \omega}{\sqrt{\hat{Q}} \hat{\omega_0}^{\frac{3}{2}}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\hat{I} \sqrt{\hat{\rho}} \omega}{\hat{\omega_0}^{\frac{3}{2}}}$$

---

$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i U
\omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\hat{I} \sqrt{\hat{\rho}} \omega}{\hat{\omega_0}^{\frac{3}{2}}}$$
$$\hat{Q}=Q$$
$$\hat{\omega_0}=\omega_0$$
$$\hat{X}=X$$
$$\frac{i U \omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}} = \frac{i \hat{I} \sqrt{\hat{\rho}}
\omega}{\hat{\omega_0}^{\frac{3}{2}}}$$
$$\frac{i U \omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}} = \frac{i \hat{I} \sqrt{\hat{\rho}}
\omega}{\omega_{0}^{\frac{3}{2}}}$$
$$U = \hat{I} \sqrt{\hat{\rho}} \sqrt{\rho}$$
$$J = i \sqrt{C} U \omega$$
$$J = i \sqrt{C} \hat{I} \sqrt{\hat{\rho}} \omega \sqrt{\rho}$$
$$J = i \sqrt{C} \sqrt{R} \hat{I} \sqrt{\hat{R}} \omega$$
$$J = \frac{i \sqrt{R} \hat{I} \sqrt{\hat{R}}}{\sqrt{L}}$$
$$J = \frac{i \hat{I} \sqrt{\hat{R}} \sqrt{\omega_{0}}}{\sqrt{Q}}$$
$$J = \frac{\sqrt{2} i \sqrt{\pi} \hat{I} \sqrt{\hat{R}} \sqrt{f_{0}}}{\sqrt{Q}}$$
$$2 \hat R = R_{sh}$$
$$J = \frac{i \sqrt{\pi} \sqrt{R_{sh}} \hat{I} \sqrt{f_{0}}}{\sqrt{Q}}$$

### Cell amplitude. Inductive normalization

```
            Ml                          Mr
   ...  <-------->	     <----------------------------->  ...
                     L              R          C
                 ^   ^   ^      +-------+     | |
...   --+-------/ \ / \ / \-----+       +-----+ +------+--  ...
        |          v   v        +-------+     | |      |
        |                                              |
     ---+--- Cl                                     ---+--- Cr
     ---+---                                        ---+---
        |                                              |
        |    +-------+   +-------+   -----    -----    |
...   --+----+       +---+       +--(---->)--(---->)---+--  ...
             +-------+   +-------+   -----    -----
                 Rg          Rl        Eg       Eb
```
**Single cell equivalent circuit**

Writing down Kirchhoff's circuit laws for the circuit

$$I_{0} \left(i L_{0} \omega + R_{0} + R_{g} + R_{load} - \frac{i}{C_{\Sigma0} \omega}\right) + I_{l} \left(i M_{l} \omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega + \frac{i}{C_{r} \omega}\right) = U e^{i \varphi}$$
and replacing lumped element parameters with electrodynamic ones
$$U = \sqrt{8 P_g R_g}$$
$$I_{0} \left(i L_{0} \omega + R_{0} + R_{g} + R_{load} - \frac{i}{C_{\Sigma0} \omega}\right) + I_{l} \left(i M_{l}
\omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega + \frac{i}{C_{r} \omega}\right) = 2 \sqrt{2}
\sqrt{P_{g}} \sqrt{R_{g}} e^{i \varphi}$$
$$Q_0 = \frac{\omega_0 L}{R} = \frac{1}{\omega_0 C_{\Sigma} R_0}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} + R_{g} +
R_{load}\right) + I_{l} \left(i M_{l} \omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega +
\frac{i}{C_{r} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{g}} e^{i \varphi}$$
$$\chi_g = \frac{R_g}{R_0}$$
$$\chi_l = \frac{R_l}{R_0}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + I_{l} \left(i M_{l} \omega + \frac{i}{C_{l} \omega}\right) + I_{r} \left(i M_{r} \omega +
\frac{i}{C_{r} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$K_H = 2\frac{M}{\sqrt{L_1 L_2}}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + I_{l} \left(\frac{i K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}} \omega}{2} + \frac{i}{C_{l}
\omega}\right) + I_{r} \left(\frac{i K^{H}_{r} \sqrt{L_{0}} \sqrt{L_{r}} \omega}{2} + \frac{i}{C_{r} \omega}\right) = 2
\sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$K_E = 2\frac{\sqrt{C_{\Sigma 1} C_{\Sigma 2}}}{C_2}$$
$$I_{0} \left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right) + I_{l} \left(\frac{i K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}} \omega}{2} + \frac{i K^{E}_{l}}{2
\sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} \omega}\right) + I_{r} \left(\frac{i K^{H}_{r} \sqrt{L_{0}} \sqrt{L_{r}}
\omega}{2} + \frac{i K^{E}_{r}}{2 \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}r}} \omega}\right) = 2 \sqrt{2} \sqrt{P_{g}}
\sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$I = \frac{X}{\sqrt{L}}$$
$$\frac{X_{r} \left(\frac{i K^{H}_{r} \sqrt{L_{0}} \sqrt{L_{r}} \omega}{2} + \frac{i K^{E}_{r}}{2 \sqrt{C_{\Sigma0}}
\sqrt{C_{{\Sigma}r}} \omega}\right)}{\sqrt{L_{r}}} + \frac{X_{l} \left(\frac{i K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}}
\omega}{2} + \frac{i K^{E}_{l}}{2 \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} \omega}\right)}{\sqrt{L_{l}}} + \frac{X_{0}
\left(\frac{i Q_{0} R_{0} \omega}{\omega_{0}} - \frac{i Q_{0} R_{0} \omega_{0}}{\omega} + R_{0} \chi_{g} + R_{0}
\chi_{load} + R_{0}\right)}{\sqrt{L_{0}}} = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$\omega = 2 \pi f$$
$$\omega_0 = 2 \pi f_0$$
$$\frac{X_{r} \left(i \pi K^{H}_{r} \sqrt{L_{0}} \sqrt{L_{r}} f + \frac{i K^{E}_{r}}{4 \pi \sqrt{C_{\Sigma0}}
\sqrt{C_{{\Sigma}r}} f}\right)}{\sqrt{L_{r}}} + \frac{X_{l} \left(i \pi K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}} f + \frac{i
K^{E}_{l}}{4 \pi \sqrt{C_{\Sigma0}} \sqrt{C_{{\Sigma}l}} f}\right)}{\sqrt{L_{l}}} + \frac{X_{0} \left(\frac{i Q_{0}
R_{0} f}{f_{0}} - \frac{i Q_{0} R_{0} f_{0}}{f} + R_{0} \chi_{g} + R_{0} \chi_{load} + R_{0}\right)}{\sqrt{L_{0}}} = 2
\sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$f = \frac{1}{2 \pi \sqrt{C L}}$$
$$\frac{X_{r} \left(\frac{i \pi K^{E}_{r} \sqrt{L_{0}} \sqrt{L_{r}} f_{0} f_{r}}{f} + i \pi K^{H}_{r} \sqrt{L_{0}}
\sqrt{L_{r}} f\right)}{\sqrt{L_{r}}} + \frac{X_{l} \left(\frac{i \pi K^{E}_{l} \sqrt{L_{0}} \sqrt{L_{l}} f_{0}
f_{l}}{f} + i \pi K^{H}_{l} \sqrt{L_{0}} \sqrt{L_{l}} f\right)}{\sqrt{L_{l}}} + \frac{X_{0} \left(\frac{i Q_{0} R_{0}
f}{f_{0}} - \frac{i Q_{0} R_{0} f_{0}}{f} + R_{0} \chi_{g} + R_{0} \chi_{load} + R_{0}\right)}{\sqrt{L_{0}}} = 2
\sqrt{2} \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 f^{2}} + \frac{K^{H}_{l}
X_{l}}{2} + \frac{K^{H}_{r} X_{r}}{2} + \frac{Q_{0} R_{0} X_{0}}{2 \pi L_{0} f_{0}} - \frac{Q_{0} R_{0} X_{0} f_{0}}{2
\pi L_{0} f^{2}} - \frac{i R_{0} X_{0} \chi_{g}}{2 \pi L_{0} f} - \frac{i R_{0} X_{0} \chi_{load}}{2 \pi L_{0} f} -
\frac{i R_{0} X_{0}}{2 \pi L_{0} f} = - \frac{\sqrt{2} i \sqrt{P_{g}} \sqrt{R_{0}} \sqrt{\chi_{g}} e^{i \varphi}}{\pi
\sqrt{L_{0}} f}$$
$$Q_0 = \frac{2 \pi f_0 L}{R}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 f^{2}} + \frac{K^{H}_{l}
X_{l}}{2} + \frac{K^{H}_{r} X_{r}}{2} + X_{0} - \frac{X_{0} f_{0}^{2}}{f^{2}} - \frac{i X_{0} \chi_{g} f_{0}}{Q_{0} f} -
\frac{i X_{0} \chi_{load} f_{0}}{Q_{0} f} - \frac{i X_{0} f_{0}}{Q_{0} f} = - \frac{2 i \sqrt{P_{g}} \sqrt{\chi_{g}}
\sqrt{f_{0}} e^{i \varphi}}{\sqrt{\pi} \sqrt{Q_{0}} f}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 f^{2}} + \frac{K^{H}_{l}
X_{l}}{2} + \frac{K^{H}_{r} X_{r}}{2} + X_{0} - \frac{X_{0} f_{0}^{2}}{f^{2}} - \frac{i X_{0} \chi_{g} f_{0}}{Q_{0} f} -
\frac{i X_{0} \chi_{load} f_{0}}{Q_{0} f} - \frac{i X_{0} f_{0}}{Q_{0} f} = - \frac{2 i \sqrt{P_{g}} \sqrt{\chi_{g}}
\sqrt{f_{0}} e^{i \varphi}}{\sqrt{\pi} \sqrt{Q_{0}} f}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 f^{2}} + \frac{K^{H}_{l}
X_{l}}{2} + \frac{K^{H}_{r} X_{r}}{2} + X_{0} - \frac{X_{0} f_{0}^{2}}{f^{2}} - \frac{i X_{0} \chi_{g} f_{0}}{Q_{0} f} -
\frac{i X_{0} \chi_{load} f_{0}}{Q_{0} f} - \frac{i X_{0} f_{0}}{Q_{0} f} = - \frac{2 i \sqrt{P_{g}} \sqrt{\chi_{g}}
\sqrt{f_{0}} e^{i \varphi}}{\sqrt{\pi} \sqrt{Q_{0}} f}$$
This equation contains only electrodynamic parameters.

### Normalized amplitude to accelerating field conversion. Inductive normalization

$$\frac{R_{sh}}{Q}$$
$$R_{sh} = \frac{U^2}{P_{loss}}$$
$$Q = \frac{\omega_0 W}{P_{loss}}$$
$$\frac{R_{sh}}{Q} = \frac{U^{2}}{W \omega_{0}}$$
$$W = \frac{L U_0^2}{2}$$
$$\frac{R_{sh}}{Q} = \frac{2 U^{2}}{L U_{0}^{2} \omega_{0}}$$
$$U_0 = X \sqrt{C}$$
$$U = X \sqrt{C_\Sigma}$$
$$\frac{R_{sh}}{Q} = \frac{2}{L \omega_{0}}$$
$$\omega = 2 \pi f$$
$$\frac{R_{sh}}{Q} = \frac{1}{\pi L f_{0}}$$

---

$$P_{g} = \frac{E^{2}}{8 R_{g}}$$
$$E = 2 \sqrt{2} \sqrt{P_{g}} \sqrt{R_{g}}$$

---

$$i \sqrt{C_{\sum}} E \omega$$
$$Q = \frac{1}{\omega_0 C_\Sigma R}$$
$$i \sqrt{C_{\sum}} E \omega = \frac{i E \omega}{\sqrt{Q} \sqrt{R} \sqrt{\omega_{0}}}$$
$$i \sqrt{C_{\sum}} E \omega = \frac{2 \sqrt{2} i \sqrt{P_{g}} \sqrt{R_{g}} \omega}{\sqrt{Q} \sqrt{R}
\sqrt{\omega_{0}}}$$
$$\chi = \frac{R_g}{R}$$
$$i \sqrt{C_{\sum}} E \omega = \frac{2 \sqrt{2} i \sqrt{P_{g}} \sqrt{\chi} \omega}{\sqrt{Q} \sqrt{\omega_{0}}}$$
$$\omega = 2 \pi f$$
$$\omega_0 = 2 \pi f_0$$
$$2 i \pi \sqrt{C_{\sum}} E f = \frac{4 i \sqrt{\pi} \sqrt{P_{g}} \sqrt{\chi} f}{\sqrt{Q} \sqrt{f_{0}}}$$

---

$$U = j \omega L I = j \omega \sqrt{L} X$$
$$U = i \sqrt{L} X \omega$$
$$U = \frac{i \sqrt{Q} X \omega}{\sqrt{\pi} \sqrt{R_{sh}} \sqrt{f_{0}}}$$
$$\omega = 2 \pi f$$
$$\omega_0 = 2 \pi f_0$$
$$U = \frac{2 i \sqrt{\pi} \sqrt{Q} X f}{\sqrt{R_{sh}} \sqrt{f_{0}}}$$

### Beam loading. Capacitive normalization

```
             L              R          C
         ^   ^   ^      +-------+     | |
+-------/ \ / \ / \-----+       +-----+ +------+
|          V   V        +-------+     | |      |
|                                              |
|                                              |
|                       -----                  |
+----------------------(--~--)-----------------+
                        -----
                          Ib
```
**Serial scheme**

$$I \left(i L \omega + R - \frac{i}{C \omega}\right) = U$$
$$Q=\frac{\rho}{R}=\frac{\omega_0 L}{R}=\frac{1}{\omega_0 C R}$$
$$I \left(\frac{i Q R \omega}{\omega_{0}} - \frac{i Q R \omega_{0}}{\omega} + R\right) = U$$
$$- \frac{I \omega^{2}}{\omega_{0}^{2}} + I + \frac{i I \omega}{Q \omega_{0}} = \frac{i U \omega}{Q R \omega_{0}}$$
$$W=\frac{L I^2}{2}=\frac{X^2}{2}$$
$$I=\frac{X}{\sqrt{L}}$$
$$- \frac{X \omega^{2}}{\sqrt{L} \omega_{0}^{2}} + \frac{X}{\sqrt{L}} + \frac{i X \omega}{\sqrt{L} Q \omega_{0}} =
\frac{i U \omega}{Q R \omega_{0}}$$
$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i \sqrt{L} U \omega}{Q R
\omega_{0}}$$
$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i U \omega}{\sqrt{Q} \sqrt{R}
\omega_{0}^{\frac{3}{2}}}$$
$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i U \omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}}$$

```
           ~
           L
       ^   ^   ^
  +---/ \ / \ / \---+
  |      V   V      |
  |        ~        |
  |        R        |
  |    +-------+    |
  +----+       +----+
  |    +-------+    |
  |        ~        |
  |        C        |
  |       | |       |
  +-------+ +-------+
  |       | |       |
  |                 |
  |      -----      |
  +-----(---->)-----+
         -----
		   ~
		   Ib
```
**Parallel scheme**

$$\hat{U} \left(i \hat{C} \omega + \frac{1}{\hat{R}} - \frac{i}{\hat{L} \omega}\right) = \hat{I}$$
$$Q=\frac{R}{\rho}=\frac{R}{\omega_0 L}=\omega_0 C R$$
$$\hat{U} \left(- \frac{i \hat{Q} \hat{\omega_0}}{\hat{R} \omega} + \frac{i \hat{Q} \omega}{\hat{R} \hat{\omega_0}} +
\frac{1}{\hat{R}}\right) = \hat{I}$$
$$\hat{U} - \frac{\hat{U} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{U} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\hat{I} \hat{R} \omega}{\hat{Q} \hat{\omega_0}}$$
$$W=\frac{C U^2}{2}=\frac{X^2}{2}$$
$$\hat U = \frac{\hat X}{\sqrt{\hat C}}$$
$$\frac{\hat{X}}{\sqrt{\hat{C}}} - \frac{\hat{X} \omega^{2}}{\sqrt{\hat{C}} \hat{\omega_0}^{2}} + \frac{i \hat{X}
\omega}{\sqrt{\hat{C}} \hat{Q} \hat{\omega_0}} = \frac{i \hat{I} \hat{R} \omega}{\hat{Q} \hat{\omega_0}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\sqrt{\hat{C}} \hat{I} \hat{R} \omega}{\hat{Q} \hat{\omega_0}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\hat{I} \sqrt{\hat{R}} \omega}{\sqrt{\hat{Q}} \hat{\omega_0}^{\frac{3}{2}}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i \hat{I} \sqrt{\hat{\rho}} \omega}{\hat{\omega_0}^{\frac{3}{2}}}
$$

---

$$- \frac{X \omega^{2}}{\omega_{0}^{2}} + X + \frac{i X \omega}{Q \omega_{0}} = \frac{i U
\omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}}$$
$$\hat{X} - \frac{\hat{X} \omega^{2}}{\hat{\omega_0}^{2}} + \frac{i \hat{X} \omega}{\hat{Q} \hat{\omega_0}} = \frac{i
\hat{I} \sqrt{\hat{\rho}} \omega}{\hat{\omega_0}^{\frac{3}{2}}}$$
$$\hat{Q}=Q$$
$$\hat{\omega_0}=\omega_0$$
$$\hat{X}=X$$
$$\frac{i U \omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}} = \frac{i \hat{I} \sqrt{\hat{\rho}}
\omega}{\hat{\omega_0}^{\frac{3}{2}}}$$
$$\frac{i U \omega}{\omega_{0}^{\frac{3}{2}} \sqrt{\rho}} = \frac{i \hat{I} \sqrt{\hat{\rho}}
\omega}{\omega_{0}^{\frac{3}{2}}}$$
$$U = \hat{I} \sqrt{\hat{\rho}} \sqrt{\rho}$$
$$J = - \frac{i U}{\sqrt{L} \omega}$$
$$J = - \frac{i \hat{I} \sqrt{\hat{\rho}} \sqrt{\rho}}{\sqrt{L} \omega}$$
$$J = - \frac{i \sqrt{R} \hat{I} \sqrt{\hat{R}}}{\sqrt{L} \omega}$$
$$J = - \frac{i \hat{I} \sqrt{\hat{R}} \sqrt{\omega_{0}}}{\sqrt{Q} \omega}$$
$$J = - \frac{\sqrt{2} i \hat{I} \sqrt{\hat{R}} \sqrt{f_{0}}}{2 \sqrt{\pi} \sqrt{Q} f}$$
$$2 \hat R = R_{sh}$$
$$J = - \frac{i \sqrt{R_{sh}} \hat{I} \sqrt{f_{0}}}{2 \sqrt{\pi} \sqrt{Q} f}$$
