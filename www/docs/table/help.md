# Input data syntax

LinacCalc program uses simple csv table to represent accelerating section.
Each row holds single cell parameters.
Column names are described in the following table.

| Column name           |           Required           | Description           |
|:--|:--:|:--|
| `f0`  | Always | Cell natural frequency. **This value is multiplied by `constants$f0_mult` value (`1e6` by default), defined in `settings.yaml`** |
| `Q0` | Always | Cell quality factor |
| `Rsh` | Always | Cell shunt impedance for traveling wave sections and effective shunt impedance for traveling wave sections. **Per cell, not per unit length**. **This value is multiplied by `constants$Rsh_mult` value (`1e6` by default), defined in `settings.yaml`** |
| `Ke` | Optional | Electric field coupling between this cell and the next one. **This value is multiplied by `constants$Ke_mult` value (`0.01` by default), defined in `settings.yaml`** |
| `Kh` | Optional | Magnetic field coupling between this cell and the next one. **This value is multiplied by `constants$Kh_mult` value (`-0.01` by default), defined in `settings.yaml`** |
| `chi` | Optional | Cell coupling to waveguide (generator or absorbing load). `0` or no value means no connection |
| `P` | Optional | Generator supplied power. `0` means absorbing load |
| `phi` | Optional | Generator signal phase. Defaults to `0` |
| `Ib` | Optional | Beam current. Defaults to `0` |
| `Iphi` | Optional | Beam current phase. Defaults to `0` |
| `df0` | Optional | Bias of `df` for sensitivity analysis |
| `dQ0` | Optional | Bias of `dQ0` for sensitivity analysis |
| `dRsh` | Optional | Bias of `dRsh` for sensitivity analysis |
| `dKe` | Optional | Bias of `dKe` for sensitivity analysis |
| `dKh` | Optional | Bias of `dKh` for sensitivity analysis |
| `dchi` | Optional | Bias of `dchi` for sensitivity analysis |
| `dP` | Optional | Bias of `dP` for sensitivity analysis |
| `dIb` | Optional | Bias of `dIb` for sensitivity analysis |
| `dIphi` | Optional | Bias of `dIphi` for sensitivity analysis |
| `cell_width` | Optional | Bar plot cell normalized width. Defaults to `1` |

All phases are defined relative to the laboratory system.

`Ke` and `Kh` values represent connection between current cell row and the next one. Therefore, the last row value is ignored.

# Load data syntax

Load table allows defining cell termination of the section.
This is useful when simulating hybrid accelerators with accelerating sections connected to each other.

Data format is a csv table in format

| $f$ | $\Re{\hat{z}}$ | $\Im{\hat{z}}$ |
|:--|:--|:--|
|Frequency point          |Normalized impedance real part          |Normalized impedance imaginary part|

Single data points are interpolated by spline.
