## General information

LinacCalc - **lin**ear **ac**celerator **calc**ulator.

It calculates properties of the RF accelerating structures using equivalent circuit method.
Source files are avaliable at <https://gitlab.com/matsievskiysv/linaccalc.git>.
Equation derivation is available at <https://gitlab.com/matsievskiysv/linaccalc-equations.git>

## Licenses

This program uses separate licenses for code and equations.

### Code

Copyright (C) 2018 Matsievskiy S.V.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

### Equations

Creative Commons Attribution 4.0 International  (CC BY 4.0)

License summary:

- Share — copy and redistribute the material in any medium or format
- Adapt — remix, transform, and build upon the material
for any purpose, even commercially.
- Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
- You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.
- No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.

You should have received a copy of the Creative Commons Attribution 4.0 International
along with this program.  If not, see <https://creativecommons.org/licenses/by/4.0/>.
