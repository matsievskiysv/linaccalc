# Input impedance solver

Input impedance solver calculates input impedance of the section as seen from the generator power input by calculating a
normalized field distribution (described in `Static` solver documentation) and using the equation
$$\hat Z_{in} = \chi_{g}
	\left[
		1
		+
		\chi_{load}
		+
		j Q_0
		\left(
			\frac{f}{f_0} - \frac{f_0}{f}
			+
			\sum_{n=1}^{N}
			\left(
				\frac{f}{f_n}
				\frac{K^H_n}{2}
				+
				\frac{f_0}{f}
				\frac{K^E_n}{2}
			\right)
			\frac{\dot X^C_n}{\dot X^C}
		\right)
		\right]^{-1}$$

**Note:** `f0`, `Rsh`, `Ke` and `Kh` variables are multiplied by scaling factors.
See `Input` documentation page for details.

## Equation derivation

Equations may be derived in two ways: with capacitive and inductive normalizations.
For completeness sake both cases derivations are presented below.

### Capacitive normalization

$$I = X \sqrt{C}$$
$$\frac{- \frac{K^{E}_{l} X_{l}}{2} - \frac{K^{E}_{r} X_{r}}{2} - \frac{K^{H}_{l} X_{l} f^{2}}{2 f_{0} f_{l}} -
\frac{K^{H}_{r} X_{r} f^{2}}{2 f_{0} f_{r}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} + \frac{i X_{0} \chi_{g} f}{Q_{0}
f_{0}} + \frac{i X_{0} \chi_{load} f}{Q_{0} f_{0}} + \frac{i X_{0} f}{Q_{0} f_{0}}}{X_{0}} = \frac{4 i \sqrt{\pi}
\sqrt{C_{\Sigma0}} \sqrt{P_{g}} \sqrt{\chi_{g}} f e^{i \varphi}}{I_{0} \sqrt{Q_{0}} \sqrt{f_{0}}}$$
$$Q_0 = \frac{1}{2 \pi f_0 L R}$$
$$\frac{- \frac{K^{E}_{l} X_{l}}{2} - \frac{K^{E}_{r} X_{r}}{2} - \frac{K^{H}_{l} X_{l} f^{2}}{2 f_{0} f_{l}} -
\frac{K^{H}_{r} X_{r} f^{2}}{2 f_{0} f_{r}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} + \frac{i X_{0} \chi_{g} f}{Q_{0}
f_{0}} + \frac{i X_{0} \chi_{load} f}{Q_{0} f_{0}} + \frac{i X_{0} f}{Q_{0} f_{0}}}{X_{0}} = \frac{2 \sqrt{2} i
\sqrt{P_{g}} \sqrt{\chi_{g}} f e^{i \varphi}}{I_{0} Q_{0} \sqrt{R_{0}} f_{0}}$$
$$U = \sqrt{8 P_g R_g} \exp{(I \varphi)}$$
$$\frac{- \frac{K^{E}_{l} X_{l}}{2} - \frac{K^{E}_{r} X_{r}}{2} - \frac{K^{H}_{l} X_{l} f^{2}}{2 f_{0} f_{l}} -
\frac{K^{H}_{r} X_{r} f^{2}}{2 f_{0} f_{r}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} + \frac{i X_{0} \chi_{g} f}{Q_{0}
f_{0}} + \frac{i X_{0} \chi_{load} f}{Q_{0} f_{0}} + \frac{i X_{0} f}{Q_{0} f_{0}}}{X_{0}} = \frac{i U \sqrt{\chi_{g}}
f}{I_{0} Q_{0} \sqrt{R_{0}} \sqrt{R_{g}} f_{0}}$$
$$\chi_g = \frac{R_g}{R}$$
$$\frac{- \frac{K^{E}_{l} X_{l}}{2} - \frac{K^{E}_{r} X_{r}}{2} - \frac{K^{H}_{l} X_{l} f^{2}}{2 f_{0} f_{l}} -
\frac{K^{H}_{r} X_{r} f^{2}}{2 f_{0} f_{r}} - \frac{X_{0} f^{2}}{f_{0}^{2}} + X_{0} + \frac{i X_{0} \chi_{g} f}{Q_{0}
f_{0}} + \frac{i X_{0} \chi_{load} f}{Q_{0} f_{0}} + \frac{i X_{0} f}{Q_{0} f_{0}}}{X_{0}} = \frac{i U f}{I_{0} Q_{0}
R_{0} f_{0}}$$
$$U = I (Z_0 + R_g)$$
$$- \frac{K^{E}_{l} X_{l}}{2 X_{0}} - \frac{K^{E}_{r} X_{r}}{2 X_{0}} - \frac{K^{H}_{l} X_{l} f^{2}}{2 X_{0} f_{0}
f_{l}} - \frac{K^{H}_{r} X_{r} f^{2}}{2 X_{0} f_{0} f_{r}} - \frac{f^{2}}{f_{0}^{2}} + 1 + \frac{i \chi_{g} f}{Q_{0}
f_{0}} + \frac{i \chi_{load} f}{Q_{0} f_{0}} + \frac{i f}{Q_{0} f_{0}} = \frac{i f \left(R_{g} + Z_{0}\right)}{Q_{0}
R_{0} f_{0}}$$
$$\hat Z_0 = \frac{Z_0}{R_g}$$
$$- \frac{K^{E}_{l} X_{l}}{2 X_{0}} - \frac{K^{E}_{r} X_{r}}{2 X_{0}} - \frac{K^{H}_{l} X_{l} f^{2}}{2 X_{0} f_{0}
f_{l}} - \frac{K^{H}_{r} X_{r} f^{2}}{2 X_{0} f_{0} f_{r}} - \frac{f^{2}}{f_{0}^{2}} + 1 + \frac{i \chi_{g} f}{Q_{0}
f_{0}} + \frac{i \chi_{load} f}{Q_{0} f_{0}} + \frac{i f}{Q_{0} f_{0}} = \frac{i f \left(R_{g} Z^{N}_{0} +
R_{g}\right)}{Q_{0} R_{0} f_{0}}$$
$$\chi_g = \frac{R_g}{R}$$
$$- \frac{K^{E}_{l} X_{l}}{2 X_{0}} - \frac{K^{E}_{r} X_{r}}{2 X_{0}} - \frac{K^{H}_{l} X_{l} f^{2}}{2 X_{0} f_{0}
f_{l}} - \frac{K^{H}_{r} X_{r} f^{2}}{2 X_{0} f_{0} f_{r}} - \frac{f^{2}}{f_{0}^{2}} + 1 + \frac{i \chi_{g} f}{Q_{0}
f_{0}} + \frac{i \chi_{load} f}{Q_{0} f_{0}} + \frac{i f}{Q_{0} f_{0}} = \frac{i \chi_{g} f \left(Z^{N}_{0} +
1\right)}{Q_{0} f_{0}}$$
$$\frac{i K^{E}_{l} Q_{0} X_{l} f_{0}}{2 X_{0} \chi_{g} f} + \frac{i K^{E}_{r} Q_{0} X_{r} f_{0}}{2 X_{0} \chi_{g} f} + \frac{i K^{H}_{l} Q_{0} X_{l} f}{2 X_{0} \chi_{g} f_{l}} + \frac{i K^{H}_{r} Q_{0} X_{r} f}{2 X_{0} \chi_{g} f_{r}} + \frac{i Q_{0} f}{\chi_{g} f_{0}} - \frac{i Q_{0} f_{0}}{\chi_{g} f} + \frac{\chi_{load}}{\chi_{g}} + \frac{1}{\chi_{g}} = Z^{N}_{0}$$

### Inductive normalization

$$I = \frac{X}{\sqrt{L}}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 X_{0} f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 X_{0} f^{2}} +
\frac{K^{H}_{l} X_{l}}{2 X_{0}} + \frac{K^{H}_{r} X_{r}}{2 X_{0}} + 1 - \frac{f_{0}^{2}}{f^{2}} - \frac{i \chi_{g}
f_{0}}{Q_{0} f} - \frac{i \chi_{load} f_{0}}{Q_{0} f} - \frac{i f_{0}}{Q_{0} f} = - \frac{2 i \sqrt{P_{g}}
\sqrt{\chi_{g}} \sqrt{f_{0}} e^{i \varphi}}{\sqrt{\pi} I_{0} \sqrt{L_{0}} \sqrt{Q_{0}} f}$$
$$Q_0 = \frac{2 \pi f_0 L}{R}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 X_{0} f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 X_{0} f^{2}} +
\frac{K^{H}_{l} X_{l}}{2 X_{0}} + \frac{K^{H}_{r} X_{r}}{2 X_{0}} + 1 - \frac{f_{0}^{2}}{f^{2}} - \frac{i \chi_{g}
f_{0}}{Q_{0} f} - \frac{i \chi_{load} f_{0}}{Q_{0} f} - \frac{i f_{0}}{Q_{0} f} = - \frac{2 \sqrt{2} i \sqrt{P_{g}}
\sqrt{\chi_{g}} f_{0} e^{i \varphi}}{I_{0} Q_{0} \sqrt{R_{0}} f}$$
$$U = \sqrt{8 P_g R_g} \exp{(I \varphi)}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 X_{0} f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 X_{0} f^{2}} +
\frac{K^{H}_{l} X_{l}}{2 X_{0}} + \frac{K^{H}_{r} X_{r}}{2 X_{0}} + 1 - \frac{f_{0}^{2}}{f^{2}} - \frac{i \chi_{g}
f_{0}}{Q_{0} f} - \frac{i \chi_{load} f_{0}}{Q_{0} f} - \frac{i f_{0}}{Q_{0} f} = - \frac{i U \sqrt{\chi_{g}}
f_{0}}{I_{0} Q_{0} \sqrt{R_{0}} \sqrt{R_{g}} f}$$
$$\chi_g = \frac{R_g}{R}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 X_{0} f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 X_{0} f^{2}} +
\frac{K^{H}_{l} X_{l}}{2 X_{0}} + \frac{K^{H}_{r} X_{r}}{2 X_{0}} + 1 - \frac{f_{0}^{2}}{f^{2}} - \frac{i \chi_{g}
f_{0}}{Q_{0} f} - \frac{i \chi_{load} f_{0}}{Q_{0} f} - \frac{i f_{0}}{Q_{0} f} = - \frac{i U f_{0}}{I_{0} Q_{0} R_{0}
f}$$
$$U = I (Z_0 + R_g)$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 X_{0} f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 X_{0} f^{2}} +
\frac{K^{H}_{l} X_{l}}{2 X_{0}} + \frac{K^{H}_{r} X_{r}}{2 X_{0}} + 1 - \frac{f_{0}^{2}}{f^{2}} - \frac{i \chi_{g}
f_{0}}{Q_{0} f} - \frac{i \chi_{load} f_{0}}{Q_{0} f} - \frac{i f_{0}}{Q_{0} f} = - \frac{i f_{0} \left(R_{g} +
Z_{0}\right)}{Q_{0} R_{0} f}$$
$$\hat Z_0 = \frac{Z_0}{R_g}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 X_{0} f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 X_{0} f^{2}} +
\frac{K^{H}_{l} X_{l}}{2 X_{0}} + \frac{K^{H}_{r} X_{r}}{2 X_{0}} + 1 - \frac{f_{0}^{2}}{f^{2}} - \frac{i \chi_{g}
f_{0}}{Q_{0} f} - \frac{i \chi_{load} f_{0}}{Q_{0} f} - \frac{i f_{0}}{Q_{0} f} = - \frac{i f_{0} \left(R_{g}
Z^{N}_{0} + R_{g}\right)}{Q_{0} R_{0} f}$$
$$\chi_g = \frac{R_g}{R}$$
$$\frac{K^{E}_{l} X_{l} f_{0} f_{l}}{2 X_{0} f^{2}} + \frac{K^{E}_{r} X_{r} f_{0} f_{r}}{2 X_{0} f^{2}} +
\frac{K^{H}_{l} X_{l}}{2 X_{0}} + \frac{K^{H}_{r} X_{r}}{2 X_{0}} + 1 - \frac{f_{0}^{2}}{f^{2}} - \frac{i \chi_{g}
f_{0}}{Q_{0} f} - \frac{i \chi_{load} f_{0}}{Q_{0} f} - \frac{i f_{0}}{Q_{0} f} = - \frac{i \chi_{g} f_{0}
\left(Z^{N}_{0} + 1\right)}{Q_{0} f}$$
$$\frac{i K^{E}_{l} Q_{0} X_{l} f_{l}}{2 X_{0} \chi_{g} f} + \frac{i K^{E}_{r} Q_{0} X_{r} f_{r}}{2 X_{0} \chi_{g} f} +
\frac{i K^{H}_{l} Q_{0} X_{l} f}{2 X_{0} \chi_{g} f_{0}} + \frac{i K^{H}_{r} Q_{0} X_{r} f}{2 X_{0} \chi_{g} f_{0}} +
\frac{i Q_{0} f}{\chi_{g} f_{0}} - \frac{i Q_{0} f_{0}}{\chi_{g} f} + \frac{\chi_{load}}{\chi_{g}} + \frac{1}{\chi_{g}}
= Z^{N}_{0}$$
