FROM debian:bookworm AS base

# set UTF encoding
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

# install fonts and basic programs
RUN apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get install -qy --no-install-recommends \
    build-essential \
    gfortran \
    git \
    curl \
    r-base-dev \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && fc-cache -fv

FROM base AS linaccalc

ARG LCDIR=/srv/shiny-server/linaccalc

COPY . $LCDIR

RUN mkdir -p $LCDIR \
    && chmod 700 $LCDIR \
    && cd $LCDIR \
    && R --vanilla --no-save -e 'options(download.file.method="libcurl");install.packages("packrat", repos="https://cran.r-project.org");library(packrat);packrat::on();packrat::restore()'

WORKDIR $LCDIR

ENTRYPOINT ["./run.R"]
