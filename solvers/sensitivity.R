## solve equations for amplitude U for range of arguments
solveUvsCellSens <- function(funcX, funcXtoU, f, args) {
    U <- NA
    tryCatch({
        args[["f"]] <- f
        X <- solveX(funcX, args)
        args[["X"]] <- X
        U <- do.call(funcXtoU,args)
    })
    return(U)
}

randomArgs <- function(cdata, cells, vars, N, map, normal = TRUE, bound=FALSE) {
    l <- list()
    for (j in c(1:N)) {
        saved_rands <- list()
        for (v in unlist(vars)) {
            k <- vars %kbv% v
            i <- as.numeric(substr(v, nchar(k) + 1, nchar(v)))
            orig <- cdata[[k]][i]
            delta <- cdata[[unlist(map)[[k]]]][i]
            r <- 0
            if (is.null(delta) | delta == 0) {
                l[[v]] <- c(l[[v]], orig)
                next
            }
            if (normal) {
                r <- rnorm(1, 0, delta)
            } else {
                r <- runif(1, min=-delta/2, max=delta/2)
            }
            if (bound) {
                if (k %in_names% saved_rands) {
                    r <- saved_rands[[k]]
                } else {
                    saved_rands[[k]] <- r
                }
            }
            l[[v]] <- c(l[[v]], orig + r)
        }
    }
    return(data.frame(l))
}
