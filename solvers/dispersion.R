library(logging)
library(compiler)

## create Dispersion function
constructMatrixDisp <-
    function(eqs,
             cdata,
             mode = c("Dispersion_simple",
                      "Dispersion_advanced"),
             set) {
        N <- nrow(cdata)
        out_data <- matrix(data = character(),
                           nrow = N,
                           ncol = 1)

        equation <- eqs[[set]][[mode]]
        for (i in c(1:N)) {
            out_data[i, 1] <- equation
        }

        out_data[is.na(out_data)] <- 0
        return(out_data)
    }

SolverDispGenerator <-
    setRefClass(
        "SolverDisp",
        fields = list(
            equations = "environment",
            constants = "list",
            hashData = "Hash",
            funcSim = "function",
            funcAdv = "function",
            eqInitSim = "matrix",
            eqInitAdv = "matrix",
            eqSim = "matrix",
            eqAdv = "matrix",
            failed = "logical",
            compiled = "logical"
        ),
        methods = list(
            initialize = function(equations, constants, ...) {
                callSuper(equations = equations,
                          constants = constants,
                          hashData = HashGenerator(),
                          failed = TRUE,
                          compiled = FALSE,
                          ...)
            },
            process = function(data, set) {
                hashData$process(c(data, set))
                if (hashData$changed()) {
                    ## if something changed in table

                    loginfo("Constructing equations")

                    eqInitSim <<-
                        constructMatrixDisp(equations, data, "Dispersion_simple", set)
                    eqInitAdv <<-
                        constructMatrixDisp(equations, data, "Dispersion_advanced", set)

                    eqSim <<- translateEqs(eqInitSim, c("f", "phi"))
                    eqAdv <<- translateEqs(eqInitAdv, c("f", "phi"))

                    constants$cdata <<- data

                    logdebug("Baking functions")

                    failed <<- FALSE
                    compiled <<- FALSE
                    tryCatch({
                        funcSim <<- bakeFunction(eqSim, env = constants,
                                                 args = alist(... =))
                    }, warning = function(w) {
                        logwarn(paste("Backing function funcSim:", w))
                        funcSim <<- function(...) {
                            NULL
                        }
                        failed <<- TRUE
                    }, error = function(e) {
                        logerror(paste("Backing function funcSim:", e))
                        funcSim <<- function(...) {
                            NULL
                        }
                        failed <<- TRUE
                    })

                    tryCatch({
                        funcAdv <<- bakeFunction(eqAdv, env = constants,
                                                 args = alist(... =))
                    }, warning = function(w) {
                        logwarn(paste("Backing function funcAdv:", w))
                        funcAdv <<- function(...) {
                            NULL
                        }
                        failed <<- TRUE
                    }, error = function(e) {
                        logerror(paste("Backing function funcAdv:", e))
                        funcAdv <<- function(...) {
                            NULL
                        }
                        failed <<- TRUE
                    })

                    logdebug("Functions baked")
                }
            },
            compile = function() {
                logdebug("Compiling functions")
                funcSim <<- cmpfun(funcSim)
                funcAdv <<- cmpfun(funcAdv)
                compiled <<- TRUE
                logdebug("Functions compiled")
            }
        )
    )

solveDispersion <- function(func, N, f, phi) {
    dataout <- matrix(numeric(), nrow = length(phi), ncol = N)
    for (i in c(1:length(phi))) {
        dataout[i,] <- func(f = f, phi = phi[i])
    }
    dataout <- as.data.frame(dataout)
    colnames(dataout) <- c(1:N)
    dataout$phi <- phi
    return(dataout)
}

dispCutEnds <- function(data) {
    N <- length(data$f)
    middle <- N %/% 2
    dd <- abs(diff(data$f))
    li <- which(dd[1:middle] == min(dd[1:middle], na.rm = TRUE))
    ri <- which(dd[middle:N] == min(dd[middle:N], na.rm = TRUE)) + middle
    return(list(f=data$f[li:ri],phi=data$phi[li:ri]))
}
