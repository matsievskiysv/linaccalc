LinacCalc - **lin**ear **ac**celerator **calc**ulator.

It calculates properties of the RF accelerating structures using equivalent circuit method.
Source files are avaliable at <https://gitlab.com/matsievskiysv/linaccalc.git>.
Equation derivation is available at <https://gitlab.com/matsievskiysv/linaccalc-equations.git>
