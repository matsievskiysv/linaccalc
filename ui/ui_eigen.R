eigenUI <- function(id, label = "Eigen") {
    ns <- NS(id)
    sizescript <- scrsize(id)

    sidebarLayout(
        sidebarPanel(
            fluidRow(
                column(
                    width = 6,
                    numericInput(
                        inputId = ns("f0"),
                        label =  settings$lang$cell_frequency,
                        value = settings$constants$f0,
                        step = 0.1
                    )
                ),
                column(
                    width = 6,
                    selectizeInput(
                        inputId = ns("coupling"),
                        label =  settings$lang$coupling,
                        choices = c("E", "H"),
                        selected = "E"
                    )
                )
            ),
            br(),
            sliderInput(
                ns("num"),
                settings$lang$range_to_plot,
                min = 1,
                max = nrow(settings$cdata),
                value = c(1, nrow(settings$cdata)),
                step = 1
            ),
            sliderInput(
                ns("mode"),
                settings$lang$mode,
                min = 1,
                max = nrow(settings$cdata),
                value = 1,
                step = 1
            )
        ),
        mainPanel(
            navbarPage(
                title = NULL,
                position = "static-top",
                inverse = FALSE,
                theme = settings$style,
                fluid = TRUE,
                collapsible = TRUE,
                header = br(),
                footer = br(),
                tabPanel(
                    title = settings$lang$solver,
                    icon = icon("cog"),
                    position = "left",
                    tags$script(sizescript),
                    plotOutput(
                        ns("plot"),
                        width = "100%",
                        height = "100%"
                    ),
                    uiOutput(ns("freq")),
                    downloadLink(
                        ns("download_data"),
                        icon("download"),
                        settings$lang$download_data,
                        class = "btn-link"
                    ),
                    br(),
                    downloadLink(
                        ns("download_debug"),
                        icon("download"),
                        settings$lang$download_debug,
                        class = "btn-link"
                    )
                ),
                tabPanel(
                    title = settings$lang$documentation,
                    icon = icon("file"),
                    position = "left",
                    withMathJax(includeMarkdown("www/docs/eigen/help.md"))
                )
            )
        )
    )
}
